﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormularzMagazyn.aspx.cs" Inherits="Fabryka.FormularzMagazyn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        
        Zmień stan magazynu:<asp:FormView ID="FormView1" runat="server" DataKeyNames="Id_Magazynowe" 
            DataSourceID="SqlDataSource1" DefaultMode="Insert" 
            onpageindexchanging="FormView1_PageIndexChanging" CellPadding="4" 
            ForeColor="#333333">



            <EditItemTemplate>
                Id_Magazynowe:
                <asp:Label ID="Id_MagazynoweLabel1" runat="server" 
                    Text='<%# Eval("Id_Magazynowe") %>' />
                <br />
                Material:
                <asp:TextBox ID="DropDownList1" runat="server" 
                    Text='<%# Bind("Material") %>' />
                <br />
                Ilosc:
                <asp:TextBox ID="IloscTextBox" runat="server" Text='<%# Bind("Ilosc") %>' />
                <br />
                <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                    CommandName="Update" Text="Update" />
                &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                    CausesValidation="False" CommandName="Cancel" Text="Cancel" />
            </EditItemTemplate>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <InsertItemTemplate>
                Material:



        <asp:DropDownList ID="DropDownList1" runat="server" 
            DataSourceID="SqlDataSource3" SelectedValue='<%# Bind("Material") %>' DataTextField="Material" 
            DataValueField="Id_Materialu">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
            ConnectionString="<%$ ConnectionStrings:fabrykaConnectionString %>" 
            SelectCommand="SELECT [Id_Materialu], [Material] FROM [Material]">
        </asp:SqlDataSource>



                
                <br />
                Ilosc:
                <asp:TextBox ID="IloscTextBox" runat="server" Text='<%# Bind("Ilosc") %>' />
                <br />
                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                    CommandName="Insert" Text="Insert" />
                &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
                    CausesValidation="False" CommandName="Cancel" Text="Cancel" />
            </InsertItemTemplate>
            <ItemTemplate>
                Id_Magazynowe:
                <asp:Label ID="Id_MagazynoweLabel" runat="server" 
                    Text='<%# Eval("Id_Magazynowe") %>' />
                <br />
                Material:
                <asp:Label ID="MaterialLabel" runat="server" 
                    Text='<%# Bind("Material") %>' />
                <br />
                Ilosc:
                <asp:Label ID="IloscLabel" runat="server" Text='<%# Bind("Ilosc") %>' />
                <br />
                <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" 
                    CommandName="Edit" Text="Edit" />
                &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" 
                    CommandName="Delete" Text="Delete" />
                &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" 
                    CommandName="New" Text="New" />
            </ItemTemplate>
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
        </asp:FormView>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            CellPadding="4" DataKeyNames="Id_Magazynowe" DataSourceID="SqlDataSource1" 
            ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:BoundField DataField="Id_Magazynowe" HeaderText="Id_Magazynowe" 
                    InsertVisible="False" ReadOnly="True" SortExpression="Id_Magazynowe" />
                <asp:BoundField DataField="Material" HeaderText="Material" 
                    SortExpression="Material" />
                <asp:BoundField DataField="Ilosc" HeaderText="Ilosc" SortExpression="Ilosc" />
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
    
    </div>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:fabrykaConnectionString %>" 
        DeleteCommand="delete_material" DeleteCommandType="StoredProcedure" 
        InsertCommand="add_material" InsertCommandType="StoredProcedure" 
        SelectCommand="get_material" SelectCommandType="StoredProcedure" 
        UpdateCommand="UPDATE [Magazyn] SET [Id_Materialu] = (select Id_Materialu from Material M where M.Material=@Material),
         [Ilosc] = @Ilosc WHERE [Id_Magazynowe] = @Id_Magazynowe">
        <DeleteParameters>
            <asp:Parameter Name="Id_Magazynowe" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Material" Type="Int32" />
            <asp:Parameter Name="Ilosc" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Id_Magazynowe" Type="Int32" />
            <asp:Parameter Name="Material" Type="String" />
            <asp:Parameter Name="Ilosc" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:Button ID="Button2" runat="server" Height="47px" onclick="Button2_Click" 
        Text="Przejdź do stanu materiałów" />
    <asp:Button ID="Button1" runat="server" Height="47px" onclick="Button1_Click1" 
        Text="Wyloguj" Width="102px" />
    <br />
    </form>
</body>
</html>
