﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormularzPracownikow.aspx.cs" Inherits="Fabryka.FormularzPracownikow" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:GridView ID="GridView1" runat="server" AllowSorting="True" 
            AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" 
            BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="Id_Zlecenia" 
            DataSourceID="SqlDataSource1">
            <Columns>
                <asp:BoundField DataField="Id_Zlecenia" HeaderText="Id_Zlecenia" 
                    InsertVisible="False" ReadOnly="True" SortExpression="Id_Zlecenia" />
                <asp:BoundField DataField="Material" HeaderText="Material" 
                    SortExpression="Material" />
                <asp:BoundField DataField="Maszyna" HeaderText="Maszyna" 
                    SortExpression="Maszyna" />
                <asp:BoundField DataField="Opakowanie" HeaderText="Opakowanie" 
                    SortExpression="Opakowanie" />
                <asp:BoundField DataField="Od" HeaderText="Od" SortExpression="Od" />
                <asp:BoundField DataField="Do" HeaderText="Do" SortExpression="Do" />
                <asp:BoundField DataField="Ilosc" HeaderText="Ilosc" SortExpression="Ilosc" />
                <asp:BoundField DataField="Status" HeaderText="Status" 
                    SortExpression="Status" />
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:fabrykaConnectionString %>" 
            SelectCommand="select_zlecenie" SelectCommandType="StoredProcedure">
        </asp:SqlDataSource>
        <asp:Button ID="Button1" runat="server" Height="43px" onclick="Button1_Click" 
            Text="Wróć" Width="77px" />
    
    </div>
    </form>
</body>
</html>
