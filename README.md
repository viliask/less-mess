System „Less-mess”

Cel projektu:
Celem mojej aplikacji jest zarządzanie systemem zleceń w firmie zajmującej się przetwórstwem lub obróbką materiału. System ma również pomóc z zarządzaniem magazynem oraz przedstawić informacje na temat zleceń pracownikom produkcji.

Środowisko:

![env](https://bitbucket.org/ziemniaki666/less-mess/raw/71634f053252ad3368c8184104a114563f13ce9b/img/env.jpg)

System korzysta z bazy danych stworzonej i zarządzanej w SQL Server Management Studio 2012. Sama aplikacja została napisana przy pomocy Visual Studio 2010 Professional w języku C# oraz technologii ASP.NET. 

Schemat tabel: 

![schema](https://bitbucket.org/ziemniaki666/less-mess/raw/71634f053252ad3368c8184104a114563f13ce9b/img/schema.jpg)

Opis funkcjonalności:

System wita nas formularzem logowania:

![func1](https://bitbucket.org/ziemniaki666/less-mess/raw/71634f053252ad3368c8184104a114563f13ce9b/img/func1.jpg)
 
Formularz Kierownika i Magazyniera obsługuje sesję.  

![func2](https://bitbucket.org/ziemniaki666/less-mess/raw/71634f053252ad3368c8184104a114563f13ce9b/img/func2.jpg)

Po zalogowaniu się do panelu kierownika mamy dostęp do formularza:

![func3](https://bitbucket.org/ziemniaki666/less-mess/raw/71634f053252ad3368c8184104a114563f13ce9b/img/func3.jpg)

Strona kierownika pozwala na dodanie nowego zlecenia, edycji istniejących oraz ich usuwanie. Przycisk nawigacyjny do wylogowania.
Strona startowa magazyniera:

![func4](https://bitbucket.org/ziemniaki666/less-mess/raw/71634f053252ad3368c8184104a114563f13ce9b/img/func4.jpg)

Magazynier używając tego formularza zmieniać stan magazynu poprzez dodawanie, edycję oraz usunięcie.
Klikając „Przejdź do stanu materiałów” zostajemy przekierowani na stronę edycji istniejących zasobów:

![func5](https://bitbucket.org/ziemniaki666/less-mess/raw/71634f053252ad3368c8184104a114563f13ce9b/img/func5.jpg)

Formularz przedstawia tabele istniejących zasobów oraz przyciski nawigacyjne: powrót do magazynu oraz wylogowanie.

Po przejściu na stronę pracowników przedstawiona jest tabela zleceń:

![func6](https://bitbucket.org/ziemniaki666/less-mess/raw/71634f053252ad3368c8184104a114563f13ce9b/img/func6.jpg)

Przykładowe funkcje:
Select:
USE [fabryka]
GO
/****** Object:  StoredProcedure [dbo].[select_zlecenie]    Script Date: 2017-01-18 22:19:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[select_zlecenie]
as
begin
SELECT [Id_Zlecenia], Material, [Maszyna], [Opakowanie], [Od], [Do], [Ilosc], [Status] FROM [Zlecenia]
JOIN [Material] M ON M.Id_Materialu=Zlecenia.Id_Materialu
JOIN [Maszyny] Ma ON Ma.Id_Maszyny=Zlecenia.Id_Maszyny
JOIN [Opakowania] O ON O.Id_Opakowania=Zlecenia.Id_Opakowania
JOIN [Status] S ON S.Id_Statusu=Zlecenia.Id_Statusu
end

Update:
UPDATE [Zlecenia] SET 
                [Id_Materialu] = (select Id_Materialu from Material M where M.Material=@Material), 
                [Id_Maszyny] = (select Id_Maszyny from Maszyny M where M.Maszyna=@Maszyna),
                [Id_Opakowania] = (select Id_Opakowania from Opakowania O where O.Opakowanie=@Opakowanie), 
                [Od] = @Od, [Do] = @Do, [Ilosc] = @Ilosc,
                [Id_Statusu] = (select Id_Statusu from Status S where S.Status=@Status)
                WHERE [Id_Zlecenia] = @Id_Zlecenia">


Insert:
USE [fabryka]
GO
/****** Object:  StoredProcedure [dbo].[insert_zlecenie]    Script Date: 2017-01-18 22:20:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[insert_zlecenie]
@Material as int,
@Maszyna as int,
@Opakowanie as int,
@Od as datetime,
@Do as datetime,
@Ilosc as int,
@Status as int
as
begin
INSERT INTO [Zlecenia] ([Id_Materialu], [Id_Maszyny], [Id_Opakowania], [Od], [Do], [Ilosc], [Id_Statusu]) 
VALUES (@Material, @Maszyna, @Opakowanie, @Od, @Do, @Ilosc, @status)
End

Delete:
USE [fabryka]
GO
/****** Object:  StoredProcedure [dbo].[delete_zlecenie]    Script Date: 2017-01-18 22:21:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[delete_zlecenie]
@Id_Zlecenia as int
as
begin
DELETE FROM [Zlecenia] WHERE [Id_Zlecenia] = @Id_Zlecenia
end