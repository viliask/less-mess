﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="dodajMaszyny.aspx.cs" Inherits="Fabryka.dodajMaszyny" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table>

    <td>
        <asp:FormView ID="FormView1" runat="server" DataKeyNames="Id_Maszyny" 
            DataSourceID="SqlDataSource1" DefaultMode="Insert" 
            style="text-align: justify" BackColor="White" BorderColor="#CCCCCC" 
            BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Both">
            <EditItemTemplate>
                Id_Maszyny:
                <asp:Label ID="Id_MaszynyLabel1" runat="server" 
                    Text='<%# Eval("Id_Maszyny") %>' />
                <br />
                Maszyna:
                <asp:TextBox ID="MaszynaTextBox" runat="server" Text='<%# Bind("Maszyna") %>' />
                <br />
                <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                    CommandName="Update" Text="Update" />
                &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                    CausesValidation="False" CommandName="Cancel" Text="Cancel" />
            </EditItemTemplate>
            <EditRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <InsertItemTemplate>
                Maszyna:
                <asp:TextBox ID="MaszynaTextBox" runat="server" Text='<%# Bind("Maszyna") %>' />
                <br />
                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                    CommandName="Insert" Text="Insert" />
                &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
                    CausesValidation="False" CommandName="Cancel" Text="Cancel" />
            </InsertItemTemplate>
            <ItemTemplate>
                Id_Maszyny:
                <asp:Label ID="Id_MaszynyLabel" runat="server" 
                    Text='<%# Eval("Id_Maszyny") %>' />
                <br />
                Maszyna:
                <asp:Label ID="MaszynaLabel" runat="server" Text='<%# Bind("Maszyna") %>' />
                <br />

                <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" 
                    CommandName="Edit" Text="Edit" />
                &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" 
                    CommandName="Delete" Text="Delete" />
                &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" 
                    CommandName="New" Text="New" />
            </ItemTemplate>
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
        </asp:FormView>
        <br />
        <br />
        <asp:GridView ID="GridView1" runat="server" AllowSorting="True" 
            AutoGenerateColumns="False" DataKeyNames="Id_Maszyny" 
            DataSourceID="SqlDataSource1" BackColor="White" BorderColor="#CCCCCC" 
            BorderStyle="None" BorderWidth="1px" CellPadding="3">
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:BoundField DataField="Id_Maszyny" HeaderText="Id_Maszyny" 
                    InsertVisible="False" ReadOnly="True" SortExpression="Id_Maszyny" />
                <asp:BoundField DataField="Maszyna" HeaderText="Maszyna" 
                    SortExpression="Maszyna" />
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
        <br />
        <br />
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:fabrykaConnectionString %>" 
            DeleteCommand="DELETE FROM [Maszyny] WHERE [Id_Maszyny] = @Id_Maszyny" InsertCommand="INSERT INTO [Maszyny] ([Maszyna]) 
VALUES (@Maszyna)" SelectCommand="SELECT * FROM [Maszyny]" 
            UpdateCommand="UPDATE [Maszyny] SET [Maszyna] = @Maszyna Where [Id_Maszyny] = @Id_Maszyny">
            <DeleteParameters>
                <asp:Parameter Name="Id_Maszyny" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Maszyna" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Maszyna" />
                <asp:Parameter Name="Id_Maszyny" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <br />
        <br />

        </td>
        <td>


            <asp:FormView ID="FormView2" runat="server" DataKeyNames="Id_Materialu" 
                DataSourceID="SqlDataSource2" DefaultMode="Insert" BackColor="White" 
                BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
                GridLines="Both">
                <EditItemTemplate>
                    Id_Materialu:
                    <asp:Label ID="Id_MaterialuLabel1" runat="server" 
                        Text='<%# Eval("Id_Materialu") %>' />
                    <br />
                    Material:
                    <asp:TextBox ID="MaterialTextBox" runat="server" 
                        Text='<%# Bind("Material") %>' />
                    <br />
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                        CommandName="Update" Text="Update" />
                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                        CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                </EditItemTemplate>
                <EditRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                <InsertItemTemplate>
                    Material:
                    <asp:TextBox ID="MaterialTextBox" runat="server" 
                        Text='<%# Bind("Material") %>' />
                    <br />
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                        CommandName="Insert" Text="Insert" />
                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
                        CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                </InsertItemTemplate>
                <ItemTemplate>
                    Id_Materialu:
                    <asp:Label ID="Id_MaterialuLabel" runat="server" 
                        Text='<%# Eval("Id_Materialu") %>' />
                    <br />
                    Material:
                    <asp:Label ID="MaterialLabel" runat="server" Text='<%# Bind("Material") %>' />
                    <br />
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" 
                        CommandName="Edit" Text="Edit" />
                    &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" 
                        CommandName="Delete" Text="Delete" />
                    &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" 
                        CommandName="New" Text="New" />
                </ItemTemplate>
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <RowStyle ForeColor="#000066" />
            </asp:FormView>
            </br>
            </br>
            <asp:GridView ID="GridView2" runat="server" AllowSorting="True" 
                AutoGenerateColumns="False" DataKeyNames="Id_Materialu" 
                DataSourceID="SqlDataSource2" BackColor="White" BorderColor="#CCCCCC" 
                BorderStyle="None" BorderWidth="1px" CellPadding="3">
                <Columns>
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                    <asp:BoundField DataField="Id_Materialu" HeaderText="Id_Materialu" 
                        InsertVisible="False" ReadOnly="True" SortExpression="Id_Materialu" />
                    <asp:BoundField DataField="Material" HeaderText="Material" 
                        SortExpression="Material" />
                </Columns>
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <RowStyle ForeColor="#000066" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#00547E" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                ConnectionString="<%$ ConnectionStrings:fabrykaConnectionString %>" 
                DeleteCommand="DELETE FROM [Material] WHERE [Id_Materialu] = @Id_Materialu" InsertCommand="INSERT INTO [Material] ([Material]) 
VALUES (@Material)" SelectCommand="Select * from Material" 
                UpdateCommand="UPDATE [Material] SET [Material] = @Material where [Id_Materialu] = @Id_Materialu">
                <DeleteParameters>
                    <asp:Parameter Name="Id_Materialu" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="Material" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Material" />
                    <asp:Parameter Name="Id_Materialu" />
                </UpdateParameters>
            </asp:SqlDataSource>


        </td>

        <td>
            <asp:FormView ID="FormView3" runat="server" DataKeyNames="Id_Opakowania" 
                DataSourceID="SqlDataSource3" DefaultMode="Insert" BackColor="White" 
                BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
                GridLines="Both">
                <EditItemTemplate>
                    Id_Opakowania:
                    <asp:Label ID="Id_OpakowaniaLabel1" runat="server" 
                        Text='<%# Eval("Id_Opakowania") %>' />
                    <br />
                    Opakowanie:
                    <asp:TextBox ID="OpakowanieTextBox" runat="server" 
                        Text='<%# Bind("Opakowanie") %>' />
                    <br />
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                        CommandName="Update" Text="Update" />
                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                        CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                </EditItemTemplate>
                <EditRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                <InsertItemTemplate>
                    Opakowanie:
                    <asp:TextBox ID="OpakowanieTextBox" runat="server" 
                        Text='<%# Bind("Opakowanie") %>' />
                    <br />
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                        CommandName="Insert" Text="Insert" />
                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
                        CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                </InsertItemTemplate>
                <ItemTemplate>
                    Id_Opakowania:
                    <asp:Label ID="Id_OpakowaniaLabel" runat="server" 
                        Text='<%# Eval("Id_Opakowania") %>' />
                    <br />
                    Opakowanie:
                    <asp:Label ID="OpakowanieLabel" runat="server" 
                        Text='<%# Bind("Opakowanie") %>' />
                    <br />
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" 
                        CommandName="Edit" Text="Edit" />
                    &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" 
                        CommandName="Delete" Text="Delete" />
                    &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" 
                        CommandName="New" Text="New" />
                </ItemTemplate>
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <RowStyle ForeColor="#000066" />
            </asp:FormView>
            </br>
            </br>
            <asp:GridView ID="GridView3" runat="server" AllowSorting="True" 
                AutoGenerateColumns="False" DataKeyNames="Id_Opakowania" 
                DataSourceID="SqlDataSource3" BackColor="White" BorderColor="#CCCCCC" 
                BorderStyle="None" BorderWidth="1px" CellPadding="3">
                <Columns>
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                    <asp:BoundField DataField="Id_Opakowania" HeaderText="Id_Opakowania" 
                        InsertVisible="False" ReadOnly="True" SortExpression="Id_Opakowania" />
                    <asp:BoundField DataField="Opakowanie" HeaderText="Opakowanie" 
                        SortExpression="Opakowanie" />
                </Columns>
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <RowStyle ForeColor="#000066" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#00547E" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                ConnectionString="<%$ ConnectionStrings:fabrykaConnectionString %>" 
                DeleteCommand="DELETE FROM [Opakowania] WHERE [Id_Opakowania] = @Id_Opakowania" InsertCommand="INSERT INTO [Opakowania] ([Opakowanie]) 
VALUES (@Opakowanie)" SelectCommand="Select * From Opakowania" 
                UpdateCommand="UPDATE [Opakowania] SET [Opakowanie] = @Opakowanie where [Id_Opakowania] = @Id_Opakowania">
                <DeleteParameters>
                    <asp:Parameter Name="Id_Opakowania" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="Opakowanie" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Opakowanie" />
                    <asp:Parameter Name="Id_Opakowania" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </td>

        </table>
        <asp:Button ID="Button1" runat="server" Text="Edytuj stan magazynu " 
            Height="36px" onclick="Button1_Click" />

        <asp:Button ID="Button2" runat="server" Height="36px" onclick="Button2_Click" 
            style="margin-top: 0px" Text="Wyloguj" Width="90px" />

    <table>
    </div>
    </form>
</body>
</html>
