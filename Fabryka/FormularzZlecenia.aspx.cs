﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Fabryka
{
    public partial class FormularzZlecenia : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["New"] == null)
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void Calendar3_SelectionChanged(object sender, EventArgs e)
        {
            DateTime dt = this.Calendar3.SelectedDate;
            this.OTextBox.Text = dt.ToString();
        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            DateTime dt = this.Calendar1.SelectedDate;
            this.DoTextBox.Text = dt.ToString();
        }


        protected void Button1_Click(object sender, EventArgs e)
        {
            SqlDataSource7.InsertParameters.Clear();
            SqlDataSource7.InsertParameters.Add("Material", DropDownList1.SelectedValue);
            SqlDataSource7.InsertParameters.Add("Maszyna", DropDownList2.SelectedValue);
            SqlDataSource7.InsertParameters.Add("Opakowanie", DropDownList3.SelectedValue);
            SqlDataSource7.InsertParameters.Add("Od", OTextBox.Text);
            SqlDataSource7.InsertParameters.Add("Do", DoTextBox.Text);
            SqlDataSource7.InsertParameters.Add("Ilosc", IloscTextBox.Text);
            SqlDataSource7.InsertParameters.Add("Status", DropDownList4.SelectedValue);
            SqlDataSource7.Insert();
        }

        protected void DropDownList4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");
        }

        









 

    }
}