﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormularzZlecenia.aspx.cs" Inherits="Fabryka.FormularzZlecenia" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        
        .style1
        {
            height: auto;
            width: 300px
        }
        
        .style2
        {
            height: auto;
            width: 154px;
        }
        .style3
        {
            height: auto;
            width: 154px;
        }
        
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <table>

    <td class="style1">
        <div style="float:left; width:201px; ">

            Dodaj nowe zlecenie:
            </br>
            
                 <div style="float:left; width:100px">Material:</div>

                            <div style="float: right; width:80px">   
                            <asp:DropDownList ID="DropDownList1" runat="server" 
                                DataSourceID="SqlDataSource2" DataTextField="Material" 
                                DataValueField="Id_Materialu">
                            </asp:DropDownList>
                            </div>
                            <div style="clear:both"></div>
                
            
                            <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:fabrykaConnectionString %>" 
                                SelectCommand="SELECT [Id_Materialu], [Material] FROM [Material]">
                            </asp:SqlDataSource>


                            <br />
                            
                                         <div style="float: left; width: 100px">Maszyna:
                                         </div>
                                         <div style="float: right; width: 80px">
                                                 <asp:DropDownList ID="DropDownList2" runat="server" 
                                                    DataSourceID="SqlDataSource3" 
                                                    DataTextField="Maszyna" DataValueField="Id_Maszyny">
                                                </asp:DropDownList>
                                         </div>
                                     <div style="clear:both"></div>                    

                        <br />
                            <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:fabrykaConnectionString %>" 
                                SelectCommand="SELECT [Id_Maszyny], [Maszyna] FROM [Maszyny]">
                            </asp:SqlDataSource>

                                             <div style="float: left; width: 100px">Opakowanie:
                                             </div>
                                             <div style="float: right; width: 80px">
                                                    <asp:DropDownList ID="DropDownList3" runat="server" 
                                                        DataSourceID="SqlDataSource4"
                                                        DataTextField="Opakowanie" 
                                                        DataValueField="Id_Opakowania">
                                                    </asp:DropDownList>
                                        </div>
                                         <div style="clear:both"></div>

                             
                            <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:fabrykaConnectionString %>" 
                                SelectCommand="SELECT [Id_Opakowania], [Opakowanie] FROM [Opakowania]">
                            </asp:SqlDataSource>
  
                            <br />

     
                         
                                  <div>
                                     <div style="float: left; width: 100px">Ilosc:
                                     </div>
                                     <div style="float: right; width: 80px">   
                                            <asp:TextBox ID="IloscTextBox" runat="server" />
                                     </div>
                                 <div style="clear:both"></div>

                        
                                            <br />

                         
                                   <div>
                                     <div style="float: left; width: 100px">Status:
                                     </div>
                                     <div style="float: right; width: 80px">
                                            <asp:DropDownList ID="DropDownList4" runat="server" 
                                                DataSourceID="SqlDataSource5"
                                                DataTextField="Status" 
                                                DataValueField="Id_Statusu" 
                                                onselectedindexchanged="DropDownList4_SelectedIndexChanged">
                                            </asp:DropDownList>
                                     </div>
                                 <div style="clear:both"></div>

                        
                                            <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:fabrykaConnectionString %>" 
                                                SelectCommand="SELECT [Id_Statusu], [Status] FROM [Status]">
                                            </asp:SqlDataSource>
      
                                            <br />
                         

                                     <div style="float: left; width: 40px">   
                                          &nbsp;
                                          <asp:Button ID="Button1" runat="server" Text="Dodaj" onclick="Button1_Click" />
                                     </div>
                                 <div style="clear:both"></div>
                        

            
                    <asp:SqlDataSource ID="SqlDataSource7" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:fabrykaConnectionString %>" 
                        InsertCommand="insert_zlecenie" InsertCommandType="StoredProcedure" 
                        SelectCommand="insert_zlecenie" SelectCommandType="StoredProcedure">
                        <InsertParameters>
                            <asp:Parameter Name="Id_Zlecenia" Type="Int32" />
                            <asp:Parameter Name="Material" Type="Int32" />
                            <asp:Parameter Name="Maszyna" Type="Int32" />
                            <asp:Parameter Name="Opakowanie" Type="Int32" />
                            <asp:Parameter Name="Od" Type="DateTime" />
                            <asp:Parameter Name="Do" Type="DateTime" />
                            <asp:Parameter Name="Ilosc" Type="Int32" />
                            <asp:Parameter Name="Status" Type="Int32" />
                        </InsertParameters>
                        <SelectParameters>
                            <asp:Parameter Name="Id_Zlecenia" Type="Int32" />
                            <asp:Parameter Name="Material" Type="Int32" />
                            <asp:Parameter Name="Maszyna" Type="Int32" />
                            <asp:Parameter Name="Opakowanie" Type="Int32" />
                            <asp:Parameter Name="Od" Type="DateTime" />
                            <asp:Parameter Name="Do" Type="DateTime" />
                            <asp:Parameter Name="Ilosc" Type="Int32" />
                            <asp:Parameter Name="Status" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>


 </td>

            
            <br />
    
    
    
    
            <br />
            <asp:SqlDataSource ID="SqlDataSource6" runat="server" 
                ConnectionString="<%$ ConnectionStrings:fabrykaConnectionString %>" 
                DeleteCommand="delete_zlecenie" DeleteCommandType="StoredProcedure" 
                InsertCommand="insert_zlecenie" InsertCommandType="StoredProcedure" 
                SelectCommand="select_zlecenie" SelectCommandType="StoredProcedure" 
                UpdateCommand="update_zlecenie" UpdateCommandType="StoredProcedure">
                <DeleteParameters>
                    <asp:Parameter Name="Id_Zlecenia" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="Id_Zlecenia" Type="Int32" />
                    <asp:Parameter Name="Material" Type="Int32" />
                    <asp:Parameter Name="Maszyna" Type="Int32" />
                    <asp:Parameter Name="Opakowanie" Type="Int32" />
                    <asp:Parameter Name="Od" Type="DateTime" />
                    <asp:Parameter Name="Do" Type="DateTime" />
                    <asp:Parameter Name="Ilosc" Type="Int32" />
                    <asp:Parameter Name="Status" Type="Int32" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Id_Zlecenia" Type="Int32" />
                    <asp:Parameter Name="Material" Type="Int32" />
                    <asp:Parameter Name="Maszyna" Type="Int32" />
                    <asp:Parameter Name="Opakowanie" Type="Int32" />
                    <asp:Parameter Name="Od" Type="DateTime" />
                    <asp:Parameter Name="Do" Type="DateTime" />
                    <asp:Parameter Name="Ilosc" Type="Int32" />
                    <asp:Parameter Name="Status" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
            
            
           
         
            
                
        
                         <td  colspan="2" class="style2">
                            <div style="float:right; width:200px; text-align: left;">
                             Od:                    
                                <asp:TextBox ID="OTextBox" runat="server" Width="112px" ></asp:TextBox>
                                                    <asp:Calendar  ID="Calendar3" runat="server"  
                                                        onselectionchanged="Calendar3_SelectionChanged" 
                                                        style="text-align: center" BackColor="White" 
                                    BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" 
                                    Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" 
                                    Width="200px">
                                                        <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                                                        <NextPrevStyle VerticalAlign="Bottom" />
                                                        <OtherMonthDayStyle ForeColor="#808080" />
                                                        <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                                        <SelectorStyle BackColor="#CCCCCC" />
                                                        <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                                                        <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                                                        <WeekendDayStyle BackColor="#FFFFCC" />
                                </asp:Calendar>
                                                        
                                
                            </div>
                        </td>
                       
                           <div style="float:right; width:200px"> 
                                
                                <td  colspan="2" class="style3">
                                Do:
                                                        <asp:TextBox ID="DoTextBox" runat="server" />
                                                        <asp:Calendar ID="Calendar1" runat="server" 
                                                        onselectionchanged="Calendar1_SelectionChanged" 
                                        BackColor="White" BorderColor="#999999" CellPadding="4" 
                                        DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" 
                                        Height="180px" Width="200px">
                                                            <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                                                            <NextPrevStyle VerticalAlign="Bottom" />
                                                            <OtherMonthDayStyle ForeColor="#808080" />
                                                            <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                                            <SelectorStyle BackColor="#CCCCCC" />
                                                            <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                                                            <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                                                            <WeekendDayStyle BackColor="#FFFFCC" />
                                    </asp:Calendar>
                                                        
                                </td>
                           

                        

                        
            </div>

        </th>
        </table>


            <br />
            <br />
            </div>


            <!-- kalendarze  
        
        
                                                                    fhggggggggggggggggggggggggggggggggjjjjj
            gfdgffgd
            -->

            

            <div>

            <asp:SqlDataSource ID="SqlDataSource9" runat="server" 
                ConnectionString="<%$ ConnectionStrings:fabrykaConnectionString %>" 
                DeleteCommand="delete_zlecenie" DeleteCommandType="StoredProcedure" 
                InsertCommand="insert_zlecenie" InsertCommandType="StoredProcedure" 
                SelectCommand="select_zlecenie" SelectCommandType="StoredProcedure" 
                UpdateCommand="UPDATE [Zlecenia] SET 
                [Id_Materialu] = (select Id_Materialu from Material M where M.Material=@Material), 
                [Id_Maszyny] = (select Id_Maszyny from Maszyny M where M.Maszyna=@Maszyna),
                [Id_Opakowania] = (select Id_Opakowania from Opakowania O where O.Opakowanie=@Opakowanie), 
                [Od] = @Od, [Do] = @Do, [Ilosc] = @Ilosc,
                [Id_Statusu] = (select Id_Statusu from Status S where S.Status=@Status)
                WHERE [Id_Zlecenia] = @Id_Zlecenia">
                <DeleteParameters>
                    <asp:Parameter Name="Id_Zlecenia" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="Material" Type="Int32" />
                    <asp:Parameter Name="Maszyna" Type="Int32" />
                    <asp:Parameter Name="Opakowanie" Type="Int32" />
                    <asp:Parameter Name="Od" Type="DateTime" />
                    <asp:Parameter Name="Do" Type="DateTime" />
                    <asp:Parameter Name="Ilosc" Type="Int32" />
                    <asp:Parameter Name="Status" Type="Int32" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Id_Zlecenia" Type="Int32" />
                    <asp:Parameter Name="Material" Type="String" />
                    <asp:Parameter Name="Maszyna" Type="String" />
                    <asp:Parameter Name="Opakowanie" Type="String" />
                    <asp:Parameter Name="Od" Type="String" />
                    <asp:Parameter Name="Do" Type="String" />
                    <asp:Parameter Name="Ilosc" Type="Int32" />
                    <asp:Parameter Name="Status" Type="String" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <asp:GridView ID="GridView3" runat="server" AllowSorting="True" 
                AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Id_Zlecenia" 
                DataSourceID="SqlDataSource9" ForeColor="#333333" GridLines="None">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                    <asp:BoundField DataField="Id_Zlecenia" HeaderText="Id_Zlecenia" 
                        InsertVisible="False" ReadOnly="True" SortExpression="Id_Zlecenia" />
                    <asp:BoundField DataField="Material" HeaderText="Material" 
                        SortExpression="Material" />
                    <asp:BoundField DataField="Maszyna" HeaderText="Maszyna" 
                        SortExpression="Maszyna" />
                    <asp:BoundField DataField="Opakowanie" HeaderText="Opakowanie" 
                        SortExpression="Opakowanie" />
                    <asp:BoundField DataField="Od" HeaderText="Od" SortExpression="Od" />
                    <asp:BoundField DataField="Do" HeaderText="Do" SortExpression="Do" />
                    <asp:BoundField DataField="Ilosc" HeaderText="Ilosc" SortExpression="Ilosc" />
                    <asp:BoundField DataField="Status" HeaderText="Status" 
                        SortExpression="Status" />
                </Columns>
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <SortedAscendingCellStyle BackColor="#FDF5AC" />
                <SortedAscendingHeaderStyle BackColor="#4D0000" />
                <SortedDescendingCellStyle BackColor="#FCF6C0" />
                <SortedDescendingHeaderStyle BackColor="#820000" />
            </asp:GridView>


            </div>
         
    <asp:Button ID="Button2" runat="server" Height="43px" onclick="Button2_Click" 
        Text="Wyloguj" Width="117px" />
         
    </form>

</body>
</html>
